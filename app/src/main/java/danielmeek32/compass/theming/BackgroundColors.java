package danielmeek32.compass.theming;

import java.util.Arrays;

public class BackgroundColors
{
	private static int[] colors = {0xff000000, 0xff1f1f1f, 0xff3f3f3f, 0xff5f5f5f, 0xff7f7f7f, 0xffbfbfbf};

	public static int[] getColors()
	{
		return Arrays.copyOf(BackgroundColors.colors, BackgroundColors.colors.length);
	}

	public static int getColor(int index)
	{
		return BackgroundColors.colors[index];
	}
}