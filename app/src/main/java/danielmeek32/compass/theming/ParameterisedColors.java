package danielmeek32.compass.theming;

import danielmeek32.compass.utils.Color;

public class ParameterisedColors
{
	private static int[] hues = {0, 15, 30, 45, 60, 75, 90, 120, 150, 165, 180, 188, 195, 210, 240, 270, 300, 330};

	private static int generateColor(int hue, double saturation, double value)
	{
		return Color.hsvToRgb(hue << 16 | (int) (saturation * 255.0) << 8 | (int) (value * 255.0));
	}

	public static int[] getBases()
	{
		int[] base_colors = new int[ParameterisedColors.hues.length + 1];
		base_colors[0] = ParameterisedColors.generateColor(0, 0.0, 0.5);
		for (int base = 1; base < ParameterisedColors.hues.length + 1; base++)
		{
			base_colors[base] = ParameterisedColors.generateColor(ParameterisedColors.hues[base - 1], 1.0, 1.0);
		}
		return base_colors;
	}

	public static int[] getVariants(int base)
	{
		int variant_count;
		if (base == 0)
		{
			variant_count = 9;
		}
		else if (base >= 1 && base < ParameterisedColors.hues.length + 1)
		{
			variant_count = 13;
		}
		else
		{
			throw new IllegalArgumentException();
		}

		int[] variant_colors = new int[variant_count];
		for (int variant = 0; variant < variant_count; variant++)
		{
			variant_colors[variant] = ParameterisedColors.getColor(base, variant);
		}
		return variant_colors;
	}

	public static int getColor(int base, int variant)
	{
		if (variant < 0)
		{
			throw new IllegalArgumentException();
		}

		if (base == 0 && base < ParameterisedColors.hues.length)
		{
			if (variant <= 8)
			{
				return ParameterisedColors.generateColor(0, 0.0, (1 * variant) / 8.0);
			}
			else
			{
				throw new IllegalArgumentException();
			}
		}
		else if (base >= 1 && base < ParameterisedColors.hues.length + 1)
		{
			if (variant <= 6)
			{
				return ParameterisedColors.generateColor(ParameterisedColors.hues[base - 1], 1.0, (2 + 1 * variant) / 8.0);
			}
			else if (variant <= 12)
			{
				return ParameterisedColors.generateColor(ParameterisedColors.hues[base - 1], (1 + 1 * (12 - variant)) / 8.0, 1.0);
			}
			else
			{
				throw new IllegalArgumentException();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	public static int getColor(int packed)
	{
		int base = (packed >> 8) & 255;
		int variant = packed & 255;
		return ParameterisedColors.getColor(base, variant);
	}

	public static int pack(int base, int variant)
	{
		if (base < 0 || base >= ParameterisedColors.hues.length + 1)
		{
			throw new IllegalArgumentException();
		}
		if (variant < 0 || (base == 0 && variant > 8) || (base >= 1 && base < ParameterisedColors.hues.length + 1 && variant > 12))
		{
			throw new IllegalArgumentException();
		}
		return ((base & 255) << 8) | (variant & 255);
	}

	public static int[] unpack(int packed)
	{
		int base = (packed >> 8) & 255;
		int variant = packed & 255;

		if (base < 0 || base >= ParameterisedColors.hues.length + 1)
		{
			throw new IllegalArgumentException();
		}
		if (variant < 0 || (base == 0 && variant > 8) || (base >= 1 && base < ParameterisedColors.hues.length + 1 && variant > 12))
		{
			throw new IllegalArgumentException();
		}

		return new int[]{base, variant};
	}
}