package danielmeek32.compass.theming;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class CompassTheme
{
	public static CompassTheme getTheme(Context context, String name)
	{
		Resources resources = context.getResources();
		int id = resources.getIdentifier("compass_theme_" + name, "array", context.getPackageName());
		if (id != 0)
		{
			TypedArray array = resources.obtainTypedArray(id);
			CompassTheme theme = new CompassTheme(array);
			array.recycle();
			return theme;
		}
		else
		{
			return null;
		}
	}

	public static String packColors(int[] colors)
	{
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (int color : colors)
		{
			if (!first)
			{
				builder.append(",");
			}
			first = false;

			builder.append(Integer.toString(color));
		}
		return builder.toString();
	}

	public static int[] unpackColors(String packed)
	{
		String[] parts = packed.split(",");
		int[] colors = new int[parts.length];
		for (int index = 0; index < parts.length; index++)
		{
			colors[index] = Integer.parseInt(parts[index]);
		}
		return colors;
	}

	@StringRes
	public final int name;
	public final Layer[] layers;
	public final Color[] colors;

	@SuppressLint("ResourceType")
	private CompassTheme(TypedArray array)
	{
		this.name = array.getResourceId(0, 0);

		int index = 1;

		Layer[] layers = new Layer[array.getInteger(index++, 0)];
		int[] color_indexes = new int[layers.length];
		for (int layer = 0; layer < layers.length; layer++)
		{
			int drawable = array.getResourceId(index++, 0);
			boolean rotate = array.getBoolean(index++, false);
			layers[layer] = new Layer(drawable, rotate, rotate ? array.getBoolean(index++, false) : false, -1);
			color_indexes[layer] = -1;
		}

		this.colors = new Color[array.getInteger(index++, 0)];
		for (int color = 0; color < this.colors.length; color++)
		{
			int label = array.getResourceId(index++, 0);
			int layer_index = array.getInteger(index++, 0);
			int default_base = array.getInteger(index++, 0);
			int default_variant = array.getInteger(index++, 0);
			this.colors[color] = new Color(label, default_base, default_variant, layer_index);
			color_indexes[layer_index] = color;
		}

		this.layers = new Layer[layers.length];
		for (int layer = 0; layer < layers.length; layer++)
		{
			this.layers[layer] = new Layer(layers[layer].drawable, layers[layer].rotate, layers[layer].northUp, color_indexes[layer]);
		}
	}

	public static class Layer
	{
		@DrawableRes
		public final int drawable;

		public final boolean rotate;
		public final boolean northUp;

		public final int colorIndex;

		private Layer(@DrawableRes int drawable, boolean rotate, boolean northUp, int colorIndex)
		{
			this.drawable = drawable;
			this.rotate = rotate;
			this.northUp = northUp;
			this.colorIndex = colorIndex;
		}
	}

	public static class Color
	{
		@StringRes
		public final int label;

		public final int defaultBase;
		public final int defaultVariant;

		public final int layerIndex;

		private Color(@StringRes int label, int defaultBase, int defaultVariant, int layerIndex)
		{
			this.label = label;
			this.defaultBase = defaultBase;
			this.defaultVariant = defaultVariant;
			this.layerIndex = layerIndex;
		}
	}
}