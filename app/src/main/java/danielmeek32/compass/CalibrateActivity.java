package danielmeek32.compass;

import android.content.Context;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

public class CalibrateActivity extends AppCompatActivity
{
	private SensorManager sensorManager;
	private Sensor sensor;
	private SensorEventListener sensorEventListener;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_calibrate);

		((Button) this.findViewById(R.id.hide)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				PreferenceManager.getDefaultSharedPreferences(CalibrateActivity.this).edit().putBoolean("advanced_accuracy_warning", false).apply();
				CalibrateActivity.this.finish();
			}
		});
		((Button) this.findViewById(R.id.close)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				CalibrateActivity.this.finish();
			}
		});

		this.sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
		this.sensorEventListener = new SensorEventListener()
		{
			@Override
			public void onSensorChanged(SensorEvent event)
			{
				// do nothing
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy)
			{
				if (accuracy != SensorManager.SENSOR_STATUS_UNRELIABLE && accuracy != SensorManager.SENSOR_STATUS_ACCURACY_LOW && accuracy != SensorManager.SENSOR_STATUS_NO_CONTACT)
				{
					CalibrateActivity.this.finish();
				}
			}
		};
	}

	@Override
	protected void onStart()
	{
		super.onStart();

		Drawable drawable = ((ImageView) this.findViewById(R.id.calibrate_diagram_foreground)).getDrawable();
		if (drawable instanceof AnimatedVectorDrawable && Build.VERSION.SDK_INT >= 23)
		{
			final Handler handler = new Handler(this.getMainLooper());
			((AnimatedVectorDrawable) drawable).start();
			((AnimatedVectorDrawable) drawable).registerAnimationCallback(new Animatable2.AnimationCallback()
			{
				@Override
				public void onAnimationEnd(final Drawable drawable)
				{
					handler.post(new Runnable()
					{
						@Override
						public void run()
						{
							((AnimatedVectorDrawable) drawable).start();
						}
					});
				}
			});
		}
		else if (drawable instanceof AnimatedVectorDrawableCompat)
		{
			final Handler handler = new Handler(this.getMainLooper());
			((AnimatedVectorDrawableCompat) drawable).start();
			((AnimatedVectorDrawableCompat) drawable).registerAnimationCallback(new Animatable2Compat.AnimationCallback()
			{
				@Override
				public void onAnimationEnd(final Drawable drawable)
				{
					handler.post(new Runnable()
					{
						@Override
						public void run()
						{
							((AnimatedVectorDrawableCompat) drawable).start();
						}
					});
				}
			});
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		this.sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		this.sensorManager.registerListener(this.sensorEventListener, this.sensor, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		this.sensorManager.unregisterListener(this.sensorEventListener, this.sensor);
	}
}