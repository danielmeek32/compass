package danielmeek32.compass.utils;

public class Color
{
	public static int rgbToHsv(int rgb)
	{
		double r = ((rgb >> 16) & 255) / 255.0;
		double g = ((rgb >> 8) & 255) / 255.0;
		double b = (rgb & 255) / 255.0;

		double h = (r == g && r == b) ? 0.0 : ((r >= g && r >= b) ? ((g >= b) ? ((g - b) / (r - b) * 60.0) : ((b - g) / (r - g) * -60.0 + 360.0)) : ((g >= b) ? ((r >= b) ? ((r - b) / (g - b) * -60.0 + 120.0) : ((b - r) / (g - r) * 60.0 + 120.0)) : ((r >= g) ? ((r - g) / (b - g) * 60.0 + 240.0) : ((g - r) / (b - r) * -60.0 + 240.0))));
		double s = 1.0 - Math.min(Math.min(r, g), b) / Math.max(Math.max(r, g), b);
		double v = Math.max(Math.max(r, g), b);

		return (int) Math.round(h) << 16 | (int) (s * 255.0) << 8 | (int) (v * 255.0);
	}

	public static int hsvToRgb(int hsv)
	{
		double h = (hsv >> 16) & 511;
		double s = ((hsv >> 8) & 255) / 255.0;
		double v = (hsv & 255) / 255.0;

		double r = (h <= 60.0 || h >= 300.0) ? 1.0 : ((h > 60.0 && h < 120.0) ? (1.0 - (h - 60.0) / 60.0) : ((h > 240.0 && h < 300.0) ? ((h - 240.0) / 60.0) : 0.0));
		double g = (h >= 60.0 && h <= 180.0) ? 1.0 : ((h > 180.0 && h < 240.0) ? (1.0 - (h - 180.0) / 60.0) : ((h < 60.0) ? (h / 60.0) : 0.0));
		double b = (h >= 180.0 && h <= 300.0) ? 1.0 : ((h > 300.0) ? (1.0 - (h - 300.0) / 60.0) : ((h > 120.0 && h < 180.0) ? ((h - 120.0) / 60.0) : 0.0));
		if (r == 1.0)
		{
			if (g == 0.0)
			{
				g = 1.0 - s;
				b = (r - g) * b + g;
			}
			else
			{
				b = 1.0 - s;
				g = (r - b) * g + b;
			}
		}
		else if (g == 1.0)
		{
			if (r == 0.0)
			{
				r = 1.0 - s;
				b = (g - r) * b + r;
			}
			else
			{
				b = 1.0 - s;
				r = (g - b) * r + b;
			}
		}
		else
		{
			if (r == 0.0)
			{
				r = 1.0 - s;
				g = (b - r) * g + r;
			}
			else
			{
				g = 1.0 - s;
				r = (b - g) * r + g;
			}
		}
		r *= v;
		g *= v;
		b *= v;

		return (int) (r * 255.0) << 16 | (int) (g * 255.0) << 8 | (int) (b * 255.0);
	}
}