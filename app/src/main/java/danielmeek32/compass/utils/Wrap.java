package danielmeek32.compass.utils;

public class Wrap
{
	public static double wrap(double value, double min, double max)
	{
		double result;

		double offset_value = value - min;
		if (offset_value < 0.0)
		{
			result = ((max - min) - (Math.abs(offset_value) % (max - min)) + min);
		}
		else
		{
			result = (offset_value % (max - min)) + min;
		}

		if (result == max)
		{
			result = min;
		}

		return result;
	}

	public static int wrap(int value, int min, int max)
	{
		int result;

		int offset_value = value - min;
		if (offset_value < 0)
		{
			result = ((max - min) - (Math.abs(offset_value) % (max - min)) + min);
		}
		else
		{
			result = (offset_value % (max - min)) + min;
		}

		if (result == max)
		{
			result = min;
		}

		return result;
	}
}