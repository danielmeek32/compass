package danielmeek32.compass;

import android.animation.TimeAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import danielmeek32.compass.theming.BackgroundColors;
import danielmeek32.compass.theming.CompassTheme;
import danielmeek32.compass.theming.ParameterisedColors;
import danielmeek32.compass.utils.Vector;
import danielmeek32.compass.utils.Wrap;

public class CompassActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{
	private double bearing = 0.0;

	private CompassView compassView;
	private CompassBehavior compassBehavior = null;

	private SensorManager sensorManager;
	private Sensor sensorCompass;
	private SensorEventListener sensorCompassEventListener;
	private Sensor sensorAccelerometer;
	private SensorEventListener sensorAccelerometerEventListener;
	private boolean sensorRunning;

	private boolean shownAccuracyWarning = false;

	private TimeAnimator animator;

	private float[] lastKnownField = null;
	private float[] lastKnownGravity = null;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_compass);

		this.sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
		this.sensorCompassEventListener = new SensorEventListener()
		{
			@Override
			public void onSensorChanged(SensorEvent event)
			{
				float[] field = new float[]{event.values[0], event.values[1], event.values[2]};

				if (CompassActivity.this.lastKnownField == null && (CompassActivity.this.lastKnownGravity != null || CompassActivity.this.sensorAccelerometer == null))
				{
					Vector gravity = CompassActivity.this.sensorAccelerometer != null ? new Vector(CompassActivity.this.lastKnownGravity[0], CompassActivity.this.lastKnownGravity[1], CompassActivity.this.lastKnownGravity[2]) : null;
					CompassActivity.this.bearing = CompassActivity.this.calculateBearing(new Vector(field[0], field[1], field[2]), gravity);
					CompassActivity.this.refreshDisplay();
				}

				CompassActivity.this.lastKnownField = field;
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy)
			{
				if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)    // calibrated type
				{
					if (accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE || accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW || accuracy == SensorManager.SENSOR_STATUS_NO_CONTACT)
					{
						if (PreferenceManager.getDefaultSharedPreferences(CompassActivity.this).getBoolean("advanced_accuracy_warning", CompassActivity.this.getResources().getBoolean(R.bool.preference_advanced_accuracy_warning_default)))
						{
							if (!CompassActivity.this.shownAccuracyWarning)
							{
								Intent intent = new Intent(CompassActivity.this, CalibrateActivity.class);
								CompassActivity.this.startActivity(intent);
								CompassActivity.this.shownAccuracyWarning = true;
							}
						}
					}
					else
					{
						CompassActivity.this.shownAccuracyWarning = false;
					}
				}
			}
		};
		this.sensorAccelerometerEventListener = new SensorEventListener()
		{
			@Override
			public void onSensorChanged(SensorEvent event)
			{
				float[] gravity = new float[]{event.values[0], event.values[1], event.values[2]};

				if (CompassActivity.this.lastKnownGravity == null && CompassActivity.this.lastKnownField != null)
				{
					Vector field = new Vector(CompassActivity.this.lastKnownField[0], CompassActivity.this.lastKnownField[1], CompassActivity.this.lastKnownField[2]);
					CompassActivity.this.bearing = CompassActivity.this.calculateBearing(field, new Vector(gravity[0], gravity[1], gravity[2]));
					CompassActivity.this.refreshDisplay();
				}

				CompassActivity.this.lastKnownGravity = gravity;
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy)
			{
				// do nothing
			}
		};
		this.sensorRunning = false;

		this.updateBackground();

		this.compassView = new CompassView(this);
		((FrameLayout) this.findViewById(R.id.compass_container)).addView(this.compassView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		this.updateCompassTheme();
		this.updateCompassBehavior();

		if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("appearance_show_bearing", this.getResources().getBoolean(R.bool.preference_apperance_show_bearing_default)))
		{
			this.findViewById(R.id.bearing).setVisibility(View.GONE);
		}

		((ImageButton) this.findViewById(R.id.settings)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(CompassActivity.this, SettingsActivity.class);
				CompassActivity.this.startActivity(intent);
			}
		});

		((ImageButton) this.findViewById(R.id.donate)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(CompassActivity.this.getResources().getString(R.string.donate_url)));
				CompassActivity.this.startActivity(intent);
			}
		});
		if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("donate_show_button", this.getResources().getBoolean(R.bool.preference_donate_show_button_default)))
		{
			this.findViewById(R.id.donate).setVisibility(View.GONE);
		}

		if (savedInstanceState != null)
		{
			this.bearing = savedInstanceState.getDouble("bearing", 0.0);
			this.shownAccuracyWarning = savedInstanceState.getBoolean("shown_accuracy_warning", false);
		}
		else
		{
			this.bearing = 0.0;
			this.shownAccuracyWarning = false;
		}

		this.refreshDisplay();

		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();

		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState)
	{
		super.onSaveInstanceState(outState);

		outState.putDouble("bearing", this.bearing);
		outState.putBoolean("shown_accuracy_warning", this.shownAccuracyWarning);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		this.startSensor();

		this.animator = new TimeAnimator();
		this.animator.setTimeListener(new TimeAnimator.TimeListener()
		{
			@Override
			public void onTimeUpdate(TimeAnimator timeAnimator, long totalTime, long deltaTime)
			{
				if (CompassActivity.this.compassBehavior != null && CompassActivity.this.lastKnownField != null && (CompassActivity.this.lastKnownGravity != null || CompassActivity.this.sensorAccelerometer == null))
				{
					Vector field = new Vector(CompassActivity.this.lastKnownField[0], CompassActivity.this.lastKnownField[1], CompassActivity.this.lastKnownField[2]);
					Vector gravity = CompassActivity.this.sensorAccelerometer != null ? new Vector(CompassActivity.this.lastKnownGravity[0], CompassActivity.this.lastKnownGravity[1], CompassActivity.this.lastKnownGravity[2]) : null;

					double sensor_bearing = CompassActivity.this.calculateBearing(field, gravity);

					double old_bearing = CompassActivity.this.bearing;
					CompassActivity.this.bearing = Wrap.wrap(CompassActivity.this.compassBehavior.updateBearing(CompassActivity.this.bearing, sensor_bearing, deltaTime), 0.0, 360.0);
					if (CompassActivity.this.bearing != old_bearing)
					{
						CompassActivity.this.refreshDisplay();
					}
				}
			}
		});
		this.animator.start();
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		this.stopSensor();

		this.animator.cancel();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		switch (key)
		{
			case "appearance_show_bearing":
				this.findViewById(R.id.bearing).setVisibility(sharedPreferences.getBoolean("appearance_show_bearing", this.getResources().getBoolean(R.bool.preference_apperance_show_bearing_default)) ? View.VISIBLE : View.GONE);
				break;
			case "appearance_theme":
			case "appearance_colors":
				this.updateCompassTheme();
				break;
			case "appearance_background":
				this.updateBackground();
				break;
			case "behavior_mode":
			case "behavior_scale":
				this.updateCompassBehavior();
				break;
			case "advanced_sensor_mode":
			case "advanced_use_accelerometer":
				if (this.sensorRunning)
				{
					this.stopSensor();
					this.startSensor();
				}
				break;
			case "donate_show_button":
				this.findViewById(R.id.donate).setVisibility(sharedPreferences.getBoolean("donate_show_button", this.getResources().getBoolean(R.bool.preference_donate_show_button_default)) ? View.VISIBLE : View.GONE);
				break;
		}
	}

	private void updateBackground()
	{
		this.findViewById(R.id.background).setBackgroundColor(BackgroundColors.getColor(PreferenceManager.getDefaultSharedPreferences(this).getInt("appearance_background", this.getResources().getInteger(R.integer.preference_appearance_background_default))));
	}

	private void updateCompassTheme()
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

		CompassTheme theme = CompassTheme.getTheme(this, preferences.getString("appearance_theme", this.getResources().getString(R.string.preference_appearance_theme_default)));

		String colors_string = preferences.getString("appearance_colors", null);
		int[] colors = colors_string != null ? CompassTheme.unpackColors(colors_string) : null;
		if (colors != null)
		{
			for (int index = 0; index < colors.length; index++)
			{
				colors[index] = ParameterisedColors.getColor(colors[index]);
			}
		}

		this.compassView.setTheme(theme, colors);
	}

	private void updateCompassBehavior()
	{
		double scale = PreferenceManager.getDefaultSharedPreferences(this).getInt("behavior_scale", this.getResources().getInteger(R.integer.preference_behavior_scale_default)) / 8.0;
		switch (PreferenceManager.getDefaultSharedPreferences(this).getString("behavior_mode", this.getResources().getString(R.string.preference_behavior_mode_default)))
		{
			case "raw":
				this.compassBehavior = new RawCompassBehavior(scale);
				break;
			case "smooth":
				this.compassBehavior = new SmoothCompassBehavior(scale);
				break;
			case "realistic":
				this.compassBehavior = new RealisticCompassBehavior(scale);
				break;
			case "dampened":
				this.compassBehavior = new RealisticDampenedCompassBehavior(scale);
				break;
			default:
				this.compassBehavior = null;
				break;
		}
	}

	private void refreshDisplay()
	{
		this.compassView.setBearing(CompassActivity.this.bearing);
		((TextView) this.findViewById(R.id.bearing)).setText(this.getString(R.string.activity_compass_bearing, Math.round(this.bearing)));
	}

	private void startSensor()
	{
		if (!this.sensorRunning)
		{
			this.sensorCompass = this.sensorManager.getDefaultSensor(PreferenceManager.getDefaultSharedPreferences(this).getString("advanced_sensor_mode", this.getResources().getString(R.string.preference_advanced_sensor_mode_default)).equals("calibrated") ? Sensor.TYPE_MAGNETIC_FIELD : Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
			this.sensorManager.registerListener(this.sensorCompassEventListener, this.sensorCompass, SensorManager.SENSOR_DELAY_UI);
			if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("advanced_use_accelerometer", this.getResources().getBoolean(R.bool.preference_advanced_use_accelerometer_default)))
			{
				this.sensorAccelerometer = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
				this.sensorManager.registerListener(this.sensorAccelerometerEventListener, this.sensorAccelerometer, SensorManager.SENSOR_DELAY_UI);
			}
			else
			{
				this.sensorAccelerometer = null;
			}
			this.sensorRunning = true;

			if (this.sensorCompass == null || this.sensorCompass.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED)
			{
				this.shownAccuracyWarning = false;
			}
		}
	}

	private void stopSensor()
	{
		if (this.sensorRunning)
		{
			this.sensorManager.unregisterListener(this.sensorCompassEventListener, this.sensorCompass);
			if (this.sensorAccelerometer != null)
			{
				this.sensorManager.unregisterListener(this.sensorAccelerometerEventListener, this.sensorAccelerometer);
			}
			this.sensorRunning = false;
		}
	}

	private double calculateBearing(Vector field, @Nullable Vector gravity)
	{
		double bearing;
		if (gravity != null)
		{
			gravity = new Vector(gravity);
			gravity.normalize();
			Vector gravity_down = new Vector(0.0, 0.0, 1.0);
			Vector axis = gravity.crossProduct(gravity_down);
			axis.normalize();
			double angle = Math.acos(gravity.dotProduct(gravity_down));

			Vector field_rotated = new Vector(axis);
			field_rotated.multiply(axis.dotProduct(field));
			Vector axis_cross_product_field = new Vector(axis).crossProduct(field);
			Vector axis_cross_product_field_cos_angle = new Vector(axis_cross_product_field);
			axis_cross_product_field_cos_angle.multiply(Math.cos(angle));
			Vector axis_cross_product_field_sin_angle = new Vector(axis_cross_product_field);
			axis_cross_product_field_sin_angle.multiply(Math.sin(angle));
			field_rotated.add(axis_cross_product_field_cos_angle.crossProduct(axis));
			field_rotated.add(axis_cross_product_field_sin_angle);

			bearing = field_rotated.getYaw() - 90.0;
		}
		else
		{
			bearing = field.getYaw() - 90.0;
		}

		switch (this.getWindowManager().getDefaultDisplay().getRotation())
		{
			case Surface.ROTATION_90:
				bearing = bearing + 90.0;
				break;
			case Surface.ROTATION_180:
				bearing = bearing + 180.0;
				break;
			case Surface.ROTATION_270:
				bearing = bearing + 270.0;
				break;
		}
		bearing = Wrap.wrap(bearing, 0.0, 360.0);

		return bearing;
	}

	private interface CompassBehavior
	{
		double updateBearing(double compassBearing, double sensorBearing, long timeDelta);
	}

	private static class RawCompassBehavior implements CompassBehavior
	{
		public RawCompassBehavior(double scale)
		{
			// do nothing
		}

		@Override
		public double updateBearing(double compassBearing, double sensorBearing, long timeDelta)
		{
			return sensorBearing;
		}
	}

	private static class SmoothCompassBehavior implements CompassBehavior
	{
		private static final double DISTANCE_FACTOR = 0.0025;
		private static final double MAX_ACCELERATION = 0.0005;

		private double distanceFactor;
		private double maxAcceleration;

		private double lastDistance = 0.0;

		public SmoothCompassBehavior(double scale)
		{
			this.distanceFactor = SmoothCompassBehavior.DISTANCE_FACTOR * (0.5 + scale * 1.5);
			this.maxAcceleration = SmoothCompassBehavior.MAX_ACCELERATION * (0.5 + scale * 1.5);
		}

		@Override
		public double updateBearing(double compassBearing, double sensorBearing, long timeDelta)
		{
			for (long count = 0; count < timeDelta; count++)
			{
				double distance = Wrap.wrap(sensorBearing - compassBearing, -180.0, 180.0);
				distance = distance * this.distanceFactor;
				if (distance > 0.0 && distance > this.lastDistance + this.maxAcceleration)
				{
					distance = this.lastDistance + this.maxAcceleration;
				}
				else if (distance < 0.0 && distance < this.lastDistance - this.maxAcceleration)
				{
					distance = this.lastDistance - this.maxAcceleration;
				}
				this.lastDistance = distance;
				compassBearing += distance;
			}

			return compassBearing;
		}
	}

	private static class RealisticCompassBehavior implements CompassBehavior
	{
		private static final double ACCELERATION_MIN = 0.0005;
		private static final double ACCELERATION_MAX = 0.00125;

		private static final double FRICTION_MIN = 0.000025;
		private static final double FRICTION_MAX = 0.00005;

		private double acceleration;
		private double friction;

		private double velocity = 0.0;

		public RealisticCompassBehavior(double scale)
		{
			this.acceleration = RealisticCompassBehavior.ACCELERATION_MIN + (RealisticCompassBehavior.ACCELERATION_MAX - RealisticCompassBehavior.ACCELERATION_MIN) * scale;
			this.friction = RealisticCompassBehavior.FRICTION_MIN + (RealisticCompassBehavior.FRICTION_MAX - RealisticCompassBehavior.FRICTION_MIN) * scale;
		}

		@Override
		public double updateBearing(double compassBearing, double sensorBearing, long timeDelta)
		{
			for (long count = 0; count < timeDelta; count++)
			{
				this.velocity += (Math.sin(Math.toRadians(sensorBearing)) * Math.cos(Math.toRadians(compassBearing)) - Math.cos(Math.toRadians(sensorBearing)) * Math.sin(Math.toRadians(compassBearing))) * this.acceleration;

				if (Math.abs(this.velocity) > this.friction)
				{
					this.velocity -= Math.copySign(this.friction, this.velocity);
				}
				else
				{
					this.velocity = 0.0;
				}

				compassBearing += this.velocity;
			}

			return compassBearing;
		}
	}

	private static class RealisticDampenedCompassBehavior implements CompassBehavior
	{
		private static final double ACCELERATION = 0.00075;

		private static final double FRICTION = 0.000025;

		private static final double DRAG_MIN = 0.005;
		private static final double DRAG_MAX = 0.05;

		private double drag;

		private double velocity = 0.0;

		public RealisticDampenedCompassBehavior(double scale)
		{
			this.drag = RealisticDampenedCompassBehavior.DRAG_MIN + (RealisticDampenedCompassBehavior.DRAG_MAX - RealisticDampenedCompassBehavior.DRAG_MIN) * scale * scale;
		}

		@Override
		public double updateBearing(double compassBearing, double sensorBearing, long timeDelta)
		{
			for (long count = 0; count < timeDelta; count++)
			{
				this.velocity += (Math.sin(Math.toRadians(sensorBearing)) * Math.cos(Math.toRadians(compassBearing)) - Math.cos(Math.toRadians(sensorBearing)) * Math.sin(Math.toRadians(compassBearing))) * RealisticDampenedCompassBehavior.ACCELERATION;

				this.velocity -= Math.copySign(this.drag * this.velocity * this.velocity, this.velocity);

				if (Math.abs(this.velocity) > RealisticDampenedCompassBehavior.FRICTION)
				{
					this.velocity -= Math.copySign(RealisticDampenedCompassBehavior.FRICTION, this.velocity);
				}
				else
				{
					this.velocity = 0.0;
				}

				compassBearing += this.velocity;
			}

			return compassBearing;
		}
	}
}