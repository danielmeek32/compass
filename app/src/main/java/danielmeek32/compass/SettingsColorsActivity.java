package danielmeek32.compass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import danielmeek32.compass.theming.CompassTheme;
import danielmeek32.compass.theming.ParameterisedColors;

public class SettingsColorsActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_settings_colors);

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

		final CompassTheme theme = CompassTheme.getTheme(this, preferences.getString("appearance_theme", this.getResources().getString(R.string.preference_appearance_theme_default)));

		if (savedInstanceState == null)
		{
			FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();

			String colors_string = preferences.getString("appearance_colors", null);
			int[] colors = colors_string != null ? CompassTheme.unpackColors(colors_string) : null;
			for (int index = 0; index < theme.colors.length; index++)
			{
				int[] color = (colors != null && index < colors.length) ? ParameterisedColors.unpack(colors[index]) : new int[]{theme.colors[index].defaultBase, theme.colors[index].defaultVariant};
				transaction.add(R.id.colors_container, ParameterisedColorPickerFragment.newInstance(this.getResources().getString(theme.colors[index].label), color[0], color[1]), Integer.toString(index));
			}

			transaction.commitNow();
		}

		((Button) this.findViewById(R.id.reset)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				FragmentManager manager = SettingsColorsActivity.this.getSupportFragmentManager();
				for (int index = 0; index < theme.colors.length; index++)
				{
					((ParameterisedColorPickerFragment) manager.findFragmentByTag(Integer.toString(index))).setColor(theme.colors[index].defaultBase, theme.colors[index].defaultVariant);
				}
			}
		});

		((Button) this.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				int[] colors = new int[theme.colors.length];
				FragmentManager manager = SettingsColorsActivity.this.getSupportFragmentManager();
				for (int index = 0; index < theme.colors.length; index++)
				{
					ParameterisedColorPickerFragment fragment = (ParameterisedColorPickerFragment) manager.findFragmentByTag(Integer.toString(index));
					colors[index] = ParameterisedColors.pack(fragment.getBase(), fragment.getVariant());
				}

				preferences.edit().putString("appearance_colors", CompassTheme.packColors(colors)).apply();

				SettingsColorsActivity.this.finish();
			}
		});
		((Button) this.findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				SettingsColorsActivity.this.finish();
			}
		});
	}
}