package danielmeek32.compass;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import java.util.Arrays;

public class ColorPreference extends Preference
{
	private int[] colors = new int[0];
	private ImageView[] previews = new ImageView[0];

	public ColorPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	public ColorPreference(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public ColorPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ColorPreference(Context context)
	{
		super(context);
	}

	@Override
	public void onBindViewHolder(PreferenceViewHolder holder)
	{
		super.onBindViewHolder(holder);

		this.previews = new ImageView[6];
		this.previews[0] = (ImageView) holder.findViewById(R.id.color_preview_1);
		this.previews[1] = (ImageView) holder.findViewById(R.id.color_preview_2);
		this.previews[2] = (ImageView) holder.findViewById(R.id.color_preview_3);
		this.previews[3] = (ImageView) holder.findViewById(R.id.color_preview_4);
		this.previews[4] = (ImageView) holder.findViewById(R.id.color_preview_5);
		this.previews[5] = (ImageView) holder.findViewById(R.id.color_preview_6);
		this.updatePreview();
	}

	public void setPreviewColors(int[] colors)
	{
		this.colors = Arrays.copyOf(colors, colors.length);
		this.updatePreview();
	}

	private void updatePreview()
	{
		for (int index = 0; index < this.previews.length; index++)
		{
			if (index < this.colors.length)
			{
				ShapeDrawable drawable = new ShapeDrawable(new RectShape());
				drawable.getPaint().setColor(this.colors[index] | 0xff000000);
				drawable.getPaint().setStyle(Paint.Style.FILL);
				drawable.setIntrinsicWidth(1);
				drawable.setIntrinsicHeight(1);
				this.previews[index].setImageDrawable(drawable);
				this.previews[index].setVisibility(View.VISIBLE);
			}
			else
			{
				this.previews[index].setVisibility(View.GONE);
			}
		}
	}
}