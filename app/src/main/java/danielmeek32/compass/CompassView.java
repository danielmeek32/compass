package danielmeek32.compass;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.SparseIntArray;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

import java.util.concurrent.Semaphore;

import danielmeek32.compass.theming.CompassTheme;
import danielmeek32.compass.theming.ParameterisedColors;
import danielmeek32.compass.utils.Color;
import danielmeek32.compass.utils.Wrap;

public class CompassView extends View
{
	private Context context;

	private double bearing = 0.0;

	private Layer[] layers = new Layer[0];

	private int lastKnownSize = 0;
	private boolean colorisedBitmapGenerationPending = false;
	private Semaphore colorisedBitmapGenerationLock = new Semaphore(1, true);

	public CompassView(Context context)
	{
		super(context);

		this.context = context;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawARGB(0, 0, 0, 0);

		int width = this.getWidth();
		int height = this.getHeight();
		int size = Math.min(width, height) / 2;

		if (size != this.lastKnownSize)
		{
			this.lastKnownSize = size;
			this.regenerateColorisedBitmaps();
		}

		if (!this.colorisedBitmapGenerationLock.tryAcquire())
		{
			return;
		}
		if (this.colorisedBitmapGenerationPending)
		{
			this.colorisedBitmapGenerationLock.release();
			return;
		}

		for (Layer layer : this.layers)
		{
			Drawable drawable = layer.colorised ? layer.colorisedDrawable : layer.drawable;
			if (layer.rotate)
			{
				canvas.save();
				canvas.rotate((float) (layer.northUp ? this.bearing : (360.0 - this.bearing)), width / 2, height / 2);
			}
			drawable.setBounds(width / 2 - size, height / 2 - size, width / 2 + size, height / 2 + size);
			drawable.draw(canvas);
			if (layer.rotate)
			{
				canvas.restore();
			}
		}

		this.colorisedBitmapGenerationLock.release();
	}

	public double getBearing()
	{
		return this.bearing;
	}

	public void setBearing(double bearing)
	{
		this.bearing = Wrap.wrap(bearing, 0.0, 360.0);

		this.postInvalidate();
	}

	public void setTheme(CompassTheme theme, @Nullable int[] colors)
	{
		this.layers = new Layer[theme.layers.length];
		for (int index = 0; index < theme.layers.length; index++)
		{
			CompassTheme.Layer layer = theme.layers[index];
			int color = 0;
			if (layer.colorIndex != -1)
			{
				color = (colors != null && colors.length > layer.colorIndex) ? colors[layer.colorIndex] : ParameterisedColors.getColor(theme.colors[layer.colorIndex].defaultBase, theme.colors[layer.colorIndex].defaultVariant);
			}
			this.layers[index] = new Layer(AppCompatResources.getDrawable(this.context, layer.drawable), layer.rotate, layer.northUp, layer.colorIndex != -1, color & 0x00ffffff);
		}
		this.regenerateColorisedBitmaps();
	}

	private void regenerateColorisedBitmaps()
	{
		this.colorisedBitmapGenerationPending = true;
		if (this.lastKnownSize != 0)
		{
			final int size = this.lastKnownSize * 4;

			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					CompassView.this.colorisedBitmapGenerationLock.acquireUninterruptibly();

					for (Layer layer : CompassView.this.layers)
					{
						if (layer.colorised)
						{
							layer.generateColorisedDrawable(size);
						}
					}

					CompassView.this.colorisedBitmapGenerationPending = false;
					CompassView.this.colorisedBitmapGenerationLock.release();

					CompassView.this.postInvalidate();
				}
			}).start();
		}
	}

	private class Layer
	{
		public final Drawable drawable;

		public final boolean rotate;
		public final boolean northUp;

		public Drawable colorisedDrawable;
		public final boolean colorised;
		public final int color;

		public Layer(Drawable drawable, boolean rotate, boolean northUp, boolean colorised, int color)
		{
			this.drawable = drawable;
			this.rotate = rotate;
			this.northUp = northUp;

			this.colorisedDrawable = null;
			this.colorised = colorised;
			this.color = color;
		}

		public void generateColorisedDrawable(int size)
		{
			if (this.colorised)
			{
				Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				this.drawable.setBounds(0, 0, size, size);
				this.drawable.draw(canvas);

				int color_hsv = Color.rgbToHsv(this.color);
				int color_h = (color_hsv >> 16) & 511;
				double color_s = ((color_hsv >> 8) & 255) / 255.0;
				double color_v = (color_hsv & 255) / 255.0;

				SparseIntArray cache = new SparseIntArray();
				int[] pixels = new int[size * size];
				bitmap.getPixels(pixels, 0, size, 0, 0, size, size);
				for (int index = 0; index < pixels.length; index++)
				{
					int pixel = pixels[index];
					if (((pixel >> 24) & 255) == 0)
					{
						continue;
					}

					int cache_index = cache.indexOfKey(pixel);
					int result;
					if (cache_index >= 0)
					{
						result = cache.valueAt(cache_index);
					}
					else
					{
						int pixel_hsv = Color.rgbToHsv(pixel & 0x00ffffff);
						int pixel_h = (pixel_hsv >> 16) & 511;
						double pixel_s = ((pixel_hsv >> 8) & 255) / 255.0;
						double pixel_v = (pixel_hsv & 255) / 255.0;

						if (pixel_s > 0.01)
						{
							pixel_h += color_h;
							pixel_s *= color_s;
							pixel_v *= color_v;
						}

						result = (pixel & 0xff000000) | Color.hsvToRgb(Wrap.wrap(pixel_h, 0, 360) << 16 | (int) (pixel_s * 255.0) << 8 | (int) (pixel_v * 255.0));

						cache.put(pixel, result);
					}
					pixels[index] = result;
				}
				bitmap.setPixels(pixels, 0, size, 0, 0, size, size);

				this.colorisedDrawable = new BitmapDrawable(CompassView.this.context.getResources(), bitmap);
				this.colorisedDrawable.setFilterBitmap(true);
			}
		}
	}
}