package danielmeek32.compass;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class ColorPickerView extends View
{
	private static int SWATCH_SIZE = 36;
	private static int SWATCH_MARGIN = 18;
	private static int LINEAR_SWATCH_WIDTH = 24;
	private static int LINEAR_SWATCH_HEIGHT = 36;
	private static int CHECK_SIZE = 24;

	private boolean linearMode;

	private int[] colors;
	private int selectedIndex;

	private int pendingSelection = -1;

	private ShapeDrawable swatchDrawable;
	private Drawable checkDrawable;

	private RippleDrawable rippleDrawable;
	private Drawable.Callback rippleDrawableCallback;
	private ShapeDrawable rippleMaskDrawable;
	private int rippleIndex = -1;

	private OnColorChangeListener listener = null;

	public ColorPickerView(Context context, boolean linearMode)
	{
		super(context);

		this.linearMode = linearMode;

		this.colors = new int[0];
		this.selectedIndex = -1;

		this.swatchDrawable = new ShapeDrawable(this.linearMode ? new RectShape() : new OvalShape());
		this.swatchDrawable.getPaint().setStyle(Paint.Style.FILL);
		this.checkDrawable = this.getContext().getDrawable(R.drawable.color_picker_view_selected).mutate();

		this.rippleDrawableCallback = new Drawable.Callback()
		{
			@Override
			public void invalidateDrawable(@NonNull Drawable drawable)
			{
				ColorPickerView.this.postInvalidate();
			}

			@Override
			public void scheduleDrawable(@NonNull Drawable who, @NonNull Runnable what, long when)
			{
				ColorPickerView.this.getHandler().postAtTime(what, who, when);
			}

			@Override
			public void unscheduleDrawable(@NonNull Drawable who, @NonNull Runnable what)
			{
				ColorPickerView.this.getHandler().removeCallbacks(what, who);
			}
		};
		this.rippleMaskDrawable = new ShapeDrawable(this.linearMode ? new RectShape() : new OvalShape());
		this.rippleMaskDrawable.getPaint().setColor(0xff000000);
		this.rippleMaskDrawable.getPaint().setStyle(Paint.Style.FILL);
		TypedArray array = this.getContext().obtainStyledAttributes(new int[]{R.attr.colorControlHighlight});
		this.rippleDrawable = new RippleDrawable(array.getColorStateList(0), null, this.rippleMaskDrawable);
		this.rippleDrawable.setCallback(this.rippleDrawableCallback);
		array.recycle();
	}

	public ColorPickerView(Context context, boolean linearMode, int[] colors, int selectedIndex)
	{
		this(context, linearMode);

		this.colors = Arrays.copyOf(colors, colors.length);
		this.selectedIndex = selectedIndex < colors.length ? selectedIndex : -1;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		int width_size = MeasureSpec.getSize(widthMeasureSpec);
		int width_mode = MeasureSpec.getMode(widthMeasureSpec);
		int height_size = MeasureSpec.getSize(heightMeasureSpec);
		int height_mode = MeasureSpec.getMode(heightMeasureSpec);

		if (this.linearMode)
		{
			float swatch_width = ColorPickerView.LINEAR_SWATCH_WIDTH * this.getResources().getDisplayMetrics().density;
			float swatch_height = ColorPickerView.LINEAR_SWATCH_HEIGHT * this.getResources().getDisplayMetrics().density;

			int width = (int) Math.ceil(swatch_width * this.colors.length) + this.getPaddingLeft() + this.getPaddingRight();
			int height = (int) Math.ceil(swatch_height) + this.getPaddingTop() + this.getPaddingBottom();

			if (width_mode == MeasureSpec.EXACTLY || (width_mode == MeasureSpec.AT_MOST && width > width_size))
			{
				width = width_size;
			}
			if (height_mode == MeasureSpec.EXACTLY || (height_mode == MeasureSpec.AT_MOST && height > height_size))
			{
				height = height_size;
			}

			this.setMeasuredDimension(width, height);
		}
		else
		{
			float swatch_size = ColorPickerView.SWATCH_SIZE * this.getResources().getDisplayMetrics().density;
			float swatch_margin = ColorPickerView.SWATCH_MARGIN * this.getResources().getDisplayMetrics().density;

			if (width_mode == MeasureSpec.EXACTLY && height_mode == MeasureSpec.EXACTLY)
			{
				this.setMeasuredDimension(width_size, height_size);
			}
			else if (width_mode == MeasureSpec.UNSPECIFIED && height_mode == MeasureSpec.UNSPECIFIED)
			{
				int cols = (int) Math.ceil(Math.sqrt(this.colors.length));
				int rows = (this.colors.length - 1) / cols + 1;
				int wanted_width = (int) Math.ceil(cols * swatch_size + (cols - 1) * swatch_margin) + this.getPaddingLeft() + this.getPaddingRight();
				int wanted_height = (int) Math.ceil(rows * swatch_size + (rows - 1) * swatch_margin) + this.getPaddingTop() + this.getPaddingBottom();

				this.setMeasuredDimension(wanted_width, wanted_height);
			}
			else if (width_mode == MeasureSpec.EXACTLY || width_mode == MeasureSpec.AT_MOST)    // height_mode == MeausreSpec.EXACTLY || height_mode == MeasureSpec.AT_MOST || height_mode == MeasureSpec.UNSPECIFIED
			{
				int cols = (int) (((width_size - this.getPaddingLeft() - this.getPaddingRight()) + swatch_margin) / (swatch_size + swatch_margin));
				int rows = (this.colors.length - 1) / Math.max(cols, 1) + 1;
				int wanted_width = (int) Math.ceil(cols * swatch_size + (cols - 1) * swatch_margin) + this.getPaddingLeft() + this.getPaddingRight();
				int wanted_height = (int) Math.ceil(rows * swatch_size + (rows - 1) * swatch_margin) + this.getPaddingTop() + this.getPaddingBottom();

				this.setMeasuredDimension(width_mode == MeasureSpec.EXACTLY ? width_size : Math.min(wanted_width, width_size), height_mode == MeasureSpec.EXACTLY ? height_size : (height_mode == MeasureSpec.AT_MOST ? Math.min(wanted_height, height_size) : wanted_height));
			}
			else if (height_mode == MeasureSpec.EXACTLY || height_mode == MeasureSpec.AT_MOST)    // width_mode == MeasureSpec.UNSPECIFIED
			{
				int rows = (int) (((height_size - this.getPaddingTop() - this.getPaddingBottom()) + swatch_margin) / (swatch_size + swatch_margin));
				int cols = (this.colors.length - 1) / Math.max(rows, 1) + 1;
				int wanted_width = (int) Math.ceil(cols * swatch_size + (cols - 1) * swatch_margin) + this.getPaddingLeft() + this.getPaddingRight();
				int wanted_height = (int) Math.ceil(rows * swatch_size + (rows - 1) * swatch_margin) + this.getPaddingTop() + this.getPaddingBottom();

				this.setMeasuredDimension(wanted_width, height_mode == MeasureSpec.EXACTLY ? height_size : Math.min(wanted_height, height_size));
			}
			else
			{
				throw new IllegalArgumentException();
			}
		}
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawARGB(0, 0, 0, 0);

		int width = this.getWidth() - this.getPaddingLeft() - this.getPaddingRight();
		int height = this.getHeight() - this.getPaddingTop() - this.getPaddingBottom();

		if (this.linearMode)
		{
			float swatch_width = ColorPickerView.LINEAR_SWATCH_WIDTH * this.getResources().getDisplayMetrics().density;
			float swatch_height = ColorPickerView.LINEAR_SWATCH_HEIGHT * this.getResources().getDisplayMetrics().density;
			float check_size = ColorPickerView.CHECK_SIZE * this.getResources().getDisplayMetrics().density;

			float used_width = swatch_width * this.colors.length;
			float used_height = swatch_height;

			float h_margin = (width - used_width) / 2.0f;
			float v_margin = (height - used_height) / 2.0f;

			for (int index = 0; index < this.colors.length; index++)
			{
				int color = this.colors[index];

				float x = this.getPaddingLeft() + h_margin + (this.getLayoutDirection() == LAYOUT_DIRECTION_RTL ? this.colors.length - index - 1 : index) * swatch_width + swatch_width / 2.0f;
				float y = this.getPaddingTop() + v_margin + swatch_height / 2.0f;

				this.swatchDrawable.getPaint().setColor(color | 0xff000000);
				this.swatchDrawable.setBounds((int) (x - swatch_width / 2.0f), (int) (y - swatch_height / 2.0f), (int) (x + swatch_width / 2.0f), (int) (y + swatch_height / 2.0f));
				this.swatchDrawable.draw(canvas);

				if (index == this.selectedIndex)
				{
					int mean = (((color >> 16) & 255) + ((color >> 8) & 255) + (color & 255)) / 3;
					this.checkDrawable.setTint(mean > 0x9f ? 0xff000000 : 0xffffffff);
					this.checkDrawable.setTintMode(PorterDuff.Mode.SRC_IN);
					this.checkDrawable.setBounds((int) (x - check_size / 2.0f), (int) (y - check_size / 2.0f), (int) (x + check_size / 2.0f), (int) (y + check_size / 2.0f));
					this.checkDrawable.draw(canvas);
				}

				if (index == this.rippleIndex)
				{
					this.rippleMaskDrawable.setBounds((int) (x - swatch_width / 2.0f), (int) (y - swatch_height / 2.0f), (int) (x + swatch_width / 2.0f), (int) (y + swatch_height / 2.0f));
					this.rippleDrawable.setBounds((int) (x - swatch_width / 2.0f), (int) (y - swatch_height / 2.0f), (int) (x + swatch_width / 2.0f), (int) (y + swatch_height / 2.0f));
					this.rippleDrawable.draw(canvas);
				}
			}
		}
		else
		{
			float swatch_size = ColorPickerView.SWATCH_SIZE * this.getResources().getDisplayMetrics().density;
			float swatch_margin = ColorPickerView.SWATCH_MARGIN * this.getResources().getDisplayMetrics().density;
			float check_size = ColorPickerView.CHECK_SIZE * this.getResources().getDisplayMetrics().density;

			int cols = (int) ((width + swatch_margin) / (swatch_size + swatch_margin));
			int rows = (this.colors.length - 1) / cols + 1;
			if (rows == 1)
			{
				cols = this.colors.length;
			}

			float used_width = cols * swatch_size + (cols - 1) * swatch_margin;
			float used_height = rows * swatch_size + (rows - 1) * swatch_margin;

			float h_margin = (width - used_width) / 2.0f;
			float v_margin = (height - used_height) / 2.0f;

			int col = 0;
			int row = 0;
			for (int index = 0; index < this.colors.length; index++)
			{
				int color = this.colors[index];

				float x = this.getPaddingLeft() + h_margin + (this.getLayoutDirection() == LAYOUT_DIRECTION_RTL ? cols - col - 1 : col) * (swatch_size + swatch_margin) + swatch_size / 2.0f;
				float y = this.getPaddingTop() + v_margin + row * (swatch_size + swatch_margin) + swatch_size / 2.0f;

				this.swatchDrawable.getPaint().setColor(color | 0xff000000);
				this.swatchDrawable.setBounds((int) (x - swatch_size / 2.0f), (int) (y - swatch_size / 2.0f), (int) (x + swatch_size / 2.0f), (int) (y + swatch_size / 2.0f));
				this.swatchDrawable.draw(canvas);

				if (index == this.selectedIndex)
				{
					int mean = (((color >> 16) & 255) + ((color >> 8) & 255) + (color & 255)) / 3;
					this.checkDrawable.setTint(mean > 0x9f ? 0xff000000 : 0xffffffff);
					this.checkDrawable.setTintMode(PorterDuff.Mode.SRC_IN);
					this.checkDrawable.setBounds((int) (x - check_size / 2.0f), (int) (y - check_size / 2.0f), (int) (x + check_size / 2.0f), (int) (y + check_size / 2.0f));
					this.checkDrawable.draw(canvas);
				}

				if (index == this.rippleIndex)
				{
					this.rippleMaskDrawable.setBounds((int) (x - swatch_size / 2.0f), (int) (y - swatch_size / 2.0f), (int) (x + swatch_size / 2.0f), (int) (y + swatch_size / 2.0f));
					this.rippleDrawable.setBounds((int) (x - swatch_size / 2.0f), (int) (y - swatch_size / 2.0f), (int) (x + swatch_size / 2.0f), (int) (y + swatch_size / 2.0f));
					this.rippleDrawable.draw(canvas);
				}

				col++;
				if (col == cols)
				{
					col = 0;
					row++;
				}
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		switch (event.getActionMasked())
		{
			case MotionEvent.ACTION_DOWN:
				this.pendingSelection = this.getColorIndexAtPosition(event.getX(event.getActionIndex()), event.getY(event.getActionIndex()));

				this.rippleDrawable.setHotspot(event.getX(event.getActionIndex()), event.getY(event.getActionIndex()));
				this.rippleDrawable.setState(new int[]{android.R.attr.state_enabled, android.R.attr.state_pressed});
				this.rippleIndex = this.pendingSelection;

				return true;
			case MotionEvent.ACTION_MOVE:
				if (this.getColorIndexAtPosition(event.getX(event.getActionIndex()), event.getY(event.getActionIndex())) != this.pendingSelection)
				{
					this.cancelSelection();
				}
				return true;
			case MotionEvent.ACTION_UP:
				if (this.pendingSelection != -1)
				{
					this.selectedIndex = this.pendingSelection;
					this.cancelSelection();
					this.postInvalidate();

					if (this.listener != null)
					{
						this.listener.onColorChange(this, this.selectedIndex, this.colors[this.selectedIndex]);
					}
				}
				return true;
			case MotionEvent.ACTION_CANCEL:
				if (this.pendingSelection != -1)
				{
					this.cancelSelection();
				}
				return true;
			default:
				return false;
		}
	}

	public int[] getColors()
	{
		return Arrays.copyOf(this.colors, this.colors.length);
	}

	public void setColors(int[] colors)
	{
		this.colors = Arrays.copyOf(colors, colors.length);
		if (this.selectedIndex >= colors.length)
		{
			this.selectedIndex = -1;
		}
		this.cancelSelection();
		this.requestLayout();
		this.postInvalidate();

		if (this.listener != null)
		{
			this.listener.onColorChange(this, this.selectedIndex, this.selectedIndex != -1 ? this.colors[this.selectedIndex] : 0);
		}
	}

	public int getSelectedIndex()
	{
		return this.selectedIndex;
	}

	public void setSelectedIndex(int index)
	{
		this.selectedIndex = index < this.colors.length ? index : -1;
		this.cancelSelection();
		this.postInvalidate();

		if (this.listener != null)
		{
			this.listener.onColorChange(this, this.selectedIndex, this.selectedIndex != -1 ? this.colors[this.selectedIndex] : 0);
		}
	}

	public void setOnColorChangeListener(OnColorChangeListener listener)
	{
		this.listener = listener;
	}

	private void cancelSelection()
	{
		if (this.pendingSelection != -1)
		{
			this.pendingSelection = -1;
			this.rippleDrawable.setState(new int[]{android.R.attr.state_enabled});
		}
	}

	private int getColorIndexAtPosition(float x, float y)
	{
		int width = this.getWidth() - this.getPaddingLeft() - this.getPaddingRight();
		int height = this.getHeight() - this.getPaddingTop() - this.getPaddingBottom();

		if (this.linearMode)
		{
			float swatch_width = ColorPickerView.LINEAR_SWATCH_WIDTH * this.getResources().getDisplayMetrics().density;
			float swatch_height = ColorPickerView.LINEAR_SWATCH_HEIGHT * this.getResources().getDisplayMetrics().density;

			float used_width = swatch_width * this.colors.length;
			float used_height = swatch_height;

			float h_margin = (width - used_width) / 2.0f;
			float v_margin = (height - used_height) / 2.0f;

			for (int index = 0; index < this.colors.length; index++)
			{
				float swatch_x = this.getPaddingLeft() + h_margin + (this.getLayoutDirection() == LAYOUT_DIRECTION_RTL ? this.colors.length - index - 1 : index) * swatch_width + swatch_width / 2.0f;
				float swatch_y = this.getPaddingTop() + v_margin + swatch_height / 2.0f;

				if (x > swatch_x - swatch_width / 2.0f && x < swatch_x + swatch_width / 2.0f && y > swatch_y - swatch_height / 2.0f && y < swatch_y + swatch_height / 2.0f)
				{
					return index;
				}
			}
		}
		else
		{
			float swatch_size = ColorPickerView.SWATCH_SIZE * this.getResources().getDisplayMetrics().density;
			float swatch_margin = ColorPickerView.SWATCH_MARGIN * this.getResources().getDisplayMetrics().density;

			int cols = (int) ((width + swatch_margin) / (swatch_size + swatch_margin));
			int rows = (this.colors.length - 1) / cols + 1;
			if (rows == 1)
			{
				cols = this.colors.length;
			}

			float used_width = cols * swatch_size + (cols - 1) * swatch_margin;
			float used_height = rows * swatch_size + (rows - 1) * swatch_margin;

			float h_margin = (width - used_width) / 2.0f;
			float v_margin = (height - used_height) / 2.0f;

			int col = 0;
			int row = 0;
			for (int index = 0; index < this.colors.length; index++)
			{
				float swatch_x = this.getPaddingLeft() + h_margin + (this.getLayoutDirection() == LAYOUT_DIRECTION_RTL ? cols - col - 1 : col) * (swatch_size + swatch_margin) + swatch_size / 2.0f;
				float swatch_y = this.getPaddingTop() + v_margin + row * (swatch_size + swatch_margin) + swatch_size / 2.0f;

				if (x > swatch_x - swatch_size / 2.0f && x < swatch_x + swatch_size / 2.0f && y > swatch_y - swatch_size / 2.0f && y < swatch_y + swatch_size / 2.0f)
				{
					return index;
				}

				col++;
				if (col == cols)
				{
					col = 0;
					row++;
				}
			}
		}

		return -1;
	}

	public interface OnColorChangeListener
	{
		void onColorChange(ColorPickerView view, int index, int color);
	}
}