package danielmeek32.compass;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import danielmeek32.compass.theming.ParameterisedColors;

public class ParameterisedColorPickerFragment extends Fragment
{
	private ColorPickerView baseColorPickerView;
	private ColorPickerView variantColorPickerView;

	private OnColorChangeListener listener = null;

	public static ParameterisedColorPickerFragment newInstance(@Nullable String label, int initialBase, int initialVariant)
	{
		Bundle arguments = new Bundle();
		arguments.putString("label", label);
		arguments.putInt("base", initialBase);
		arguments.putInt("variant", initialVariant);

		ParameterisedColorPickerFragment fragment = new ParameterisedColorPickerFragment();
		fragment.setArguments(arguments);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_parameterised_color_picker, container, false);

		String label = this.getArguments().getString("label");
		if (label != null)
		{
			((TextView) view.findViewById(R.id.label)).setText(label);
		}
		else
		{
			view.findViewById(R.id.label).setVisibility(View.GONE);
		}

		int base = savedInstanceState != null ? savedInstanceState.getInt("base") : this.getArguments().getInt("base");
		int variant = savedInstanceState != null ? savedInstanceState.getInt("variant") : this.getArguments().getInt("variant");

		this.baseColorPickerView = new ColorPickerView(this.getContext(), false, ParameterisedColors.getBases(), base);
		this.baseColorPickerView.setOnColorChangeListener(new ColorPickerView.OnColorChangeListener()
		{
			@Override
			public void onColorChange(ColorPickerView view, int index, int color)
			{
				int[] variants = ParameterisedColors.getVariants(index);
				int old_variant_count = ParameterisedColorPickerFragment.this.variantColorPickerView.getColors().length;
				ParameterisedColorPickerFragment.this.variantColorPickerView.setColors(variants);
				if (variants.length != old_variant_count)
				{
					ParameterisedColorPickerFragment.this.variantColorPickerView.setSelectedIndex(variants.length / 2);
				}

				if (ParameterisedColorPickerFragment.this.listener != null)
				{
					ParameterisedColorPickerFragment.this.listener.onColorChange(ParameterisedColorPickerFragment.this.baseColorPickerView.getSelectedIndex(), ParameterisedColorPickerFragment.this.variantColorPickerView.getSelectedIndex());
				}
			}
		});

		this.variantColorPickerView = new ColorPickerView(this.getContext(), true, ParameterisedColors.getVariants(base), variant);
		this.variantColorPickerView.setOnColorChangeListener(new ColorPickerView.OnColorChangeListener()
		{
			@Override
			public void onColorChange(ColorPickerView view, int index, int color)
			{
				if (ParameterisedColorPickerFragment.this.listener != null)
				{
					ParameterisedColorPickerFragment.this.listener.onColorChange(ParameterisedColorPickerFragment.this.baseColorPickerView.getSelectedIndex(), ParameterisedColorPickerFragment.this.variantColorPickerView.getSelectedIndex());
				}
			}
		});

		((FrameLayout) view.findViewById(R.id.base_container)).addView(this.baseColorPickerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		((FrameLayout) view.findViewById(R.id.variant_container)).addView(this.variantColorPickerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL));

		return view;
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState)
	{
		super.onSaveInstanceState(outState);

		outState.putInt("base", this.baseColorPickerView.getSelectedIndex());
		outState.putInt("variant", this.variantColorPickerView.getSelectedIndex());
	}

	public int getBase()
	{
		return this.baseColorPickerView.getSelectedIndex();
	}

	public int getVariant()
	{
		return this.variantColorPickerView.getSelectedIndex();
	}

	public void setColor(int base, int variant)
	{
		this.baseColorPickerView.setSelectedIndex(base);
		this.variantColorPickerView.setSelectedIndex(variant);
	}

	public void setOnColorChangeListener(OnColorChangeListener listener)
	{
		this.listener = listener;
	}

	public interface OnColorChangeListener
	{
		void onColorChange(int base, int variant);
	}
}