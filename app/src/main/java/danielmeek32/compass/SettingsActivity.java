package danielmeek32.compass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

import danielmeek32.compass.theming.BackgroundColors;
import danielmeek32.compass.theming.CompassTheme;
import danielmeek32.compass.theming.ParameterisedColors;

public class SettingsActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_settings);
		this.getSupportActionBar().setHomeButtonEnabled(true);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		this.getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, new SettingsFragment()).commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
		{
			this.onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener
	{
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
		{
			this.setPreferencesFromResource(R.xml.preferences, rootKey);

			this.updateColors();
			this.updateBackground();

			this.getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
		}

		@Override
		public void onDestroy()
		{
			super.onDestroy();

			this.getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
		{
			switch (key)
			{
				case "appearance_theme":
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.remove("appearance_colors");
					editor.apply();
					this.updateColors();
					break;
				case "appearance_colors":
					this.updateColors();
					break;
				case "appearance_background":
					this.updateBackground();
					break;
			}
		}

		private void updateColors()
		{
			SharedPreferences preferences = this.getPreferenceScreen().getSharedPreferences();

			CompassTheme theme = CompassTheme.getTheme(this.getContext(), preferences.getString("appearance_theme", this.getResources().getString(R.string.preference_appearance_theme_default)));

			int[] preview_colors = new int[theme.colors.length];
			String colors_string = preferences.getString("appearance_colors", null);
			int[] colors = colors_string != null ? CompassTheme.unpackColors(colors_string) : null;
			for (int index = 0; index < theme.colors.length; index++)
			{
				preview_colors[index] = (colors != null && index < colors.length) ? ParameterisedColors.getColor(colors[index]) : ParameterisedColors.getColor(theme.colors[index].defaultBase, theme.colors[index].defaultVariant);
			}

			ColorPreference preference = (ColorPreference) this.findPreference("appearance_colors");
			preference.setPreviewColors(preview_colors);
			preference.setEnabled(theme.colors.length > 0);
		}

		private void updateBackground()
		{
			((ColorPreference) this.findPreference("appearance_background")).setPreviewColors(new int[]{BackgroundColors.getColor(this.getPreferenceScreen().getSharedPreferences().getInt("appearance_background", this.getResources().getInteger(R.integer.preference_appearance_background_default)))});
		}
	}
}