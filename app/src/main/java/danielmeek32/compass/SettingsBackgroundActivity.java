package danielmeek32.compass;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import danielmeek32.compass.theming.BackgroundColors;

public class SettingsBackgroundActivity extends AppCompatActivity
{
	private ColorPickerView colorPickerView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_settings_background);

		((Button) this.findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				SettingsBackgroundActivity.this.finish();
			}
		});

		this.colorPickerView = new ColorPickerView(this, false, BackgroundColors.getColors(), savedInstanceState != null ? savedInstanceState.getInt("color") : PreferenceManager.getDefaultSharedPreferences(this).getInt("appearance_background", this.getResources().getInteger(R.integer.preference_appearance_background_default)));
		this.colorPickerView.setOnColorChangeListener(new ColorPickerView.OnColorChangeListener()
		{
			@Override
			public void onColorChange(ColorPickerView view, int index, int color)
			{
				PreferenceManager.getDefaultSharedPreferences(SettingsBackgroundActivity.this).edit().putInt("appearance_background", index).apply();
				SettingsBackgroundActivity.this.finish();
			}
		});

		int padding = (int) (16.0f * this.getResources().getDisplayMetrics().density);
		((FrameLayout) this.findViewById(R.id.color_picker_container)).addView(this.colorPickerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	}

	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState)
	{
		super.onSaveInstanceState(outState);

		outState.putInt("color", this.colorPickerView.getSelectedIndex());
	}
}