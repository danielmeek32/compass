# Compass

Compass with a range of styles

## Description

Open source compass with a range of styles and customisation options.

* 8 compass styles featuring simple and realistic designs with customisable colors
* A choice of smooth or realistic compass movement
* No ads or special permissions

## Donate

https://ko-fi.com/danielmeek32

## License

Code: GPL-3.0-only  
Compass styles: CC-BY-NC-SA-4.0  
